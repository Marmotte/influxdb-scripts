#! /usr/bin/env python

from influxdb import InfluxDBClient
from pynetgear import Netgear

netgear = Netgear(host='hostname', password='password')

influxdb_client = InfluxDBClient(database='house')

# Netgear device general information
netgear_info = netgear.get_info()
influxdb_client.write_points([{
    'measurement': 'netgear',
    'tags': {
        'name': netgear_info['DeviceName'],
        'model': netgear_info['ModelName'],
        'serial_number': netgear_info['SerialNumber'],
    },
    'fields': {
        'software_version': netgear_info['Firmwareversion'],
        'hardware_version': netgear_info['Hardwareversion'],
        'other_software_version': netgear_info['OthersoftwareVersion'],
        'serial_number': netgear_info['SerialNumber'],
    },
}])

# Connected devices
connected_devices = {
    device.mac: device
    for device in netgear.get_attached_devices()
}
for device in netgear.get_attached_devices_2():
    influxdb_client.write_points([{
        'measurement': 'netgear_connected_device',
        'tags': {
            'name': connected_devices[device.mac].name,
            'connection': device.type,
            'ip': device.ip,
            'mac': device.mac,
            'allowed': device.allow_or_block == 'Allow',
            'ssid': device.ssid or '',
        },
        'fields': {
            'signal_percent': connected_devices[device.mac].signal,
            'signal_db': device.signal,
            'speed': connected_devices[device.mac].link_rate or 1000,
        },
    }])
