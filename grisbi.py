#! /usr/bin/env python3
import csv
import sys

from datetime import datetime
from influxdb import InfluxDBClient


reader = csv.DictReader(open(sys.argv[1]), delimiter=';')

points = []
for row in reader:
    if row['Ventilation']:
        # We only keep main rows
        continue

    if not row['Date']:
        # Don't keep initial line
        continue

    points.append({
        'measurement': 'comptes',
        'tags': {
            'account': row['Nom du compte'],
            'third_party': row['Tiers'],
            'category': row['Catégorie'],
            'subcategory': row['Sous-catégories'],
            'reconciliation': bool(row['N° de rapprochement']),
        },
        'time': datetime.strptime(row['Date'], '%d/%m/%Y'),
        'fields': {
            'credit': float(row['Crédit'].replace(',', '')),
            'debit': float(row['Débit'].replace(',', '')),
            'balance': float(row['Solde'].replace(',', '')),
        },
    })

influxdb_client = InfluxDBClient(
    host='marnas', database='comptes',
    username='marmotte', password='marmotte')
influxdb_client.query(
    "DELETE FROM comptes "
    "WHERE account = '{account}' AND reconciliation = 'False'"
    .format(account=row['Nom du compte']))
influxdb_client.write_points(points)
