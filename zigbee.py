#! /usr/bin/env python3

import pytz
import requests

from datetime import datetime
from influxdb import InfluxDBClient

local_tz = pytz.timezone('Europe/Paris')
datetime_format = '%Y-%m-%dT%H:%M:%S.%f'

api_key = 'secret'
deconz_url = 'http://monitoring-server:8080/api/{api_key}/sensors'.format(
    api_key=api_key)

requests.get(deconz_url + '/new')
json_data = requests.get(deconz_url).json()

measurements = {
    'ZHATemperature': 'temperature',
    'ZHAHumidity': 'humidity',
    'ZHAPressure': 'pressure',
}
points = []
for index, data in json_data.items():
    if data['type'] not in measurements:
        continue

    data_time = pytz.utc.localize(datetime.strptime(
        data['state']['lastupdated'], datetime_format,
    ))
    local_data_time = local_tz.normalize(data_time)

    new_point = {
        'measurement': measurements[data['type']],
        'tags': {
            'address': 'address',
            'room': data['name'],
        },
        'time': data_time,
        'fields': {
            'value': data['state'][measurements[data['type']]],
        },
    }
    if data['config'].get('battery') is not None:
        new_point['fields']['battery'] = data['config']['battery']

    points.append(new_point)

influxdb_client = InfluxDBClient(database='house')
influxdb_client.write_points(points)
