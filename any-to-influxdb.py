#! /usr/bin/env python3

import pytz

from datetime import datetime
from flask import Flask, request
from flask_restful import Resource, Api
from influxdb import InfluxDBClient


class Backup(Resource):
    def post(self):
        """ Get JSON formatted data from borg info """
        all_data = request.get_json()
        data = all_data
        archives_count = None
        if isinstance(all_data, list):
            data = all_data[0]
            archives_count = len(all_data[1]['archives'])

        repo_data = data['repository']
        datetime_format = '%Y-%m-%dT%H:%M:%S.%f'
        influxdb_client = InfluxDBClient(database='house')

        for archive_data in data['archives']:
            data_time = pytz.timezone('Europe/Paris').localize(
                datetime.strptime(archive_data['start'], datetime_format))

            location = repo_data['location']
            if location.startswith('/'):
                location = 'file://' + location

            influxdb_client.write_points([{
                'measurement': 'backup',
                'tags': {
                    'hostname': archive_data['hostname'],
                    'repo': location,
                },
                'time': data_time,
                'fields': {
                    'duration': archive_data['duration'],
                    'files_count': archive_data['stats']['nfiles'],
                    'compressed_size': archive_data['stats']['compressed_size'],
                    'deduplicated_size': archive_data['stats']['deduplicated_size'],
                    'original_size': archive_data['stats']['original_size'],
                    'total_size': data['cache']['stats']['unique_csize'],
                    'archives_count': archives_count,
                },
            }])

        return 'Measures added in InfluxDB'


if __name__ == '__main__':
    app = Flask(__name__)
    api = Api(app)
    api.add_resource(Backup, '/backup')
    app.run(host='0.0.0.0')
