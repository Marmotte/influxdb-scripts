#! /usr/bin/env python3

from influxdb import InfluxDBClient
from nut2 import PyNUTClient

nut = PyNUTClient()

influxdb_client = InfluxDBClient(database='house')
for ups_name, ups_description in nut.list_ups().items():
    ups_vars = nut.list_vars(ups_name)

    if ups_vars['device.type'] != 'ups':
        continue

    influxdb_client.write_points([{
        'measurement': 'ups',
        'tags': {
            'name': ups_name,
            'description': ups_description,
            'battery_type': ups_vars['battery.type'],
            'manufacturer': ups_vars['device.mfr'],
            'model': ups_vars['device.model'],
            'status': ups_vars['ups.status'],
        },
        'fields': {
            'battery_charge': float(ups_vars['battery.charge']),
            'battery_charge_low': float(ups_vars['battery.charge.low']),
            'battery_runtime': float(ups_vars['battery.runtime']),
            'input_frequency': float(ups_vars['input.frequency']),
            'input_transfer_high': float(ups_vars['input.transfer.high']),
            'input_transfer_low': float(ups_vars['input.transfer.low']),
            'input_voltage': float(ups_vars['input.voltage']),
            'output_frequency': float(ups_vars['output.frequency']),
            'output_frequency_nominal': float(ups_vars['output.frequency.nominal']),
            'output_voltage': float(ups_vars['output.voltage']),
            'output_voltage_nominal': float(ups_vars['output.voltage.nominal']),
            'delay_shutdown': float(ups_vars['ups.delay.shutdown']),
            'delay_start': float(ups_vars['ups.delay.start']),
            'load': float(ups_vars['ups.load']),
            'power': float(ups_vars['ups.power']),
            'power_nominal': float(ups_vars['ups.power.nominal']),
            'power_real': float(ups_vars['ups.realpower']),
            'timer_shutdown': float(ups_vars['ups.timer.shutdown']),
            'timer_start': float(ups_vars['ups.timer.start']),
        },
    }])
