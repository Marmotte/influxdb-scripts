#! /usr/bin/env python3

import pytz

from datetime import datetime, timedelta
from influxdb import InfluxDBClient
from pylinky import LinkyClient

local_tz = pytz.timezone('Europe/Paris')
heures_creuses = [
    (12, 30),
    (13, 0),
    (13, 30),
    (14, 0),
    (14, 30),
    (15, 0),
    (2, 0),
    (2, 30),
    (3, 0),
    (3, 30),
    (4, 0),
    (4, 30),
    (5, 0),
    (5, 30),
    (6, 0),
    (6, 30),
]
start = datetime.now() - timedelta(days=3)
end = datetime.now()
datetime_format = '%Y-%m-%dT%H:%M:%SZ'
username = 'username'
password = 'password'

linky_client = LinkyClient(username=username, password=password)
linky_client.login()

linky_raw_data = linky_client.get_data_per_period(start=start, end=end)
linky_formatted_data = linky_client.format_data(
    linky_raw_data, time_format=datetime_format)

influxdb_client = InfluxDBClient(database='house')
for data in linky_formatted_data:
    data_time = pytz.utc.localize(datetime.strptime(
        data['time'], datetime_format,
    ))
    local_data_time = local_tz.normalize(data_time)
    local_time_tuple = (local_data_time.hour, local_data_time.minute)

    influxdb_client.write_points([{
        'measurement': 'enedis',
        'tags': {
            'address': 'address',
            'price': (
                'Heures creuses'
                if local_time_tuple in heures_creuses
                else 'Heures pleines'
            )
        },
        'time': data_time,
        'fields': {
            'conso': float(data['conso']),
        },
    }])
