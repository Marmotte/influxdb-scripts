#! /usr/bin/env python3

from influxdb import InfluxDBClient
from huawei_lte_api.Client import Client
from huawei_lte_api.AuthorizedConnection import AuthorizedConnection

client = Client(AuthorizedConnection(
    'http://admin:password@192.168.2.2'))

influxdb_client = InfluxDBClient(database='house')

month_statistics = client.monitoring.month_statistics()
month_information = client.monitoring.start_date()
traffic_statistics = client.monitoring.traffic_statistics()
device_status = client.monitoring.status()
device_information = client.device.information()

influxdb_client.write_points([{
    'measurement': 'flybox',
    'tags': {
        'name': device_information['DeviceName'],
        'serial': device_information['SerialNumber'],
        'wan_ip': device_information['WanIPAddress'],
        'hardware_version': device_information['HardwareVersion'],
        'software_version': device_information['SoftwareVersion'],
        'webui_version': device_information['WebUIVersion'],
    },
    'fields': {
        'wan_ip': device_information['WanIPAddress'],
        'month_download': int(month_statistics['CurrentMonthDownload']),
        'month_upload': int(month_statistics['CurrentMonthUpload']),
        'month_duration': int(month_statistics['MonthDuration']),
        'month_limit': int(month_information['trafficmaxlimit']),
        'uptime': int(traffic_statistics['CurrentConnectTime']),
        'total_uptime': int(traffic_statistics['TotalConnectTime']),
        'download': int(traffic_statistics['CurrentDownload']),
        'upload': int(traffic_statistics['CurrentUpload']),
        'total_download': int(traffic_statistics['TotalDownload']),
        'total_upload': int(traffic_statistics['TotalUpload']),
        'download_rate': int(traffic_statistics['CurrentDownloadRate']),
        'upload_rate': int(traffic_statistics['CurrentUploadRate']),
        'wifi_users': int(device_status['CurrentWifiUser']),
    },
}])

client.user.logout()
