#! /usr/bin/env python

import pytz

from datetime import datetime, timedelta
from influxdb import InfluxDBClient
from sysbus import sysbus

datetime_format = '%Y-%m-%dT%H:%M:%SZ'

sysbus.load_conf()
sysbus.auth()

influxdb_client = InfluxDBClient(database='house')
points = []

# Livebox device general information
data_time = datetime.now(pytz.utc)
livebox_info = sysbus.requete('DeviceInfo:get')['status']
points.append({
    'measurement': 'livebox',
    'tags': {
        'status': livebox_info['DeviceStatus'],
        'manufacturer': livebox_info['Manufacturer'],
        'product': livebox_info['ProductClass'],
        'model': livebox_info['ModelName'],
        'country': livebox_info['Country'],
    },
    'time': data_time,
    'fields': {
        'status': livebox_info['DeviceStatus'],
        'uptime': livebox_info['UpTime'],
        'reboots': livebox_info['NumberOfReboots'],
        'public_ip': livebox_info['ExternalIPAddress'],
        'hardware_version': livebox_info['HardwareVersion'],
        'software_version': livebox_info['SoftwareVersion'],
        'rescue_version': livebox_info['RescueVersion'],
        'provisioning_code': livebox_info['ProvisioningCode'],
        'serial_number': livebox_info['SerialNumber'],
        'spec_version': livebox_info['SpecVersion'],
    },
})

# Phonecalls
phonecalls = sysbus.requete(
    'VoiceService.VoiceApplication:getCallList')['status']
for phonecall in phonecalls:
    data_time = pytz.utc.localize(datetime.strptime(
        phonecall['startTime'], datetime_format))
    points.append({
        'measurement': 'phonecalls',
        'tags': {
            'direction': (
                'Appel reçu'
                if phonecall['callDestination'] == 'local'
                else 'Appel émis'
            ),
            'status': phonecall['callType'],
            'remote_number': phonecall['remoteNumber'],
        },
        'time': data_time,
        'fields': {
            'duration': phonecall['duration'],
            'remote_name': phonecall['remoteName'],
            'remote_number': phonecall['remoteNumber'],
            'status': phonecall['callType'],
            'direction': (
                'Appel reçu'
                if phonecall['callDestination'] == 'local'
                else 'Appel émis'
            ),
        },
    })

# Local network
data_time = datetime.now(pytz.utc)
ethernet_interfaces = sysbus.requete(
    'NeMo.Intf.data:getMIBs', {'traverse': 'all', 'mibs': 'eth'})['status']
for port, interface in ethernet_interfaces['eth'].items():
    points.append({
        'measurement': 'livebox_ethernet',
        'tags': {
            'status': 'Up' if interface['CurrentBitRate'] > 0 else 'Down',
            'interface': interface['PhyDevice'],
            'port': port,
        },
        'time': data_time,
        'fields': {
            'status': 'Up' if interface['CurrentBitRate'] > 0 else 'Down',
            'speed': interface['CurrentBitRate'],
            'max_speed': interface['MaxBitRateSupported'],
            'uptime': interface['LastChange'],
        },
    })

data_time = datetime.now(pytz.utc)
for device in sysbus.requete('Devices:get')['status']:
    device_tags = device['Tags'].split()
    if 'lan' not in device_tags:
        continue

    uptime = data_time - pytz.utc.localize(datetime.strptime(
        device['LastChanged'], datetime_format))
    tags = {
        'status': 'Up' if device['Active'] else 'Down',
        'name': device['Name'],
        'interface': device.get('NetDevName', device.get('InterfaceName', '')),
    }
    fields = {
        'status': 'Up' if device['Active'] else 'Down',
        'uptime': int(uptime.total_seconds()) if device['Active'] else 0,
    }

    if 'self' not in device_tags or 'bridge' in device_tags:
        # Device connected to the local network
        points.append({
            'measurement': 'network_device',
            'tags': dict(
                tags,
                ip=device.get('IPAddress', ''),
                mac=device['PhysAddress'],
            ),
            'time': data_time,
            'fields': dict(
                fields,
            ),
        })
    elif 'self' in device_tags and 'wifi' in device_tags:
        # Wifi interface
        points.append({
            'measurement': 'livebox_wifi',
            'tags': dict(
                tags,
                ssid=device['SSID'],
                bssid=device['BSSID'],
                band=device['OperatingFrequencyBand'],
                standards=device['OperatingStandards'],
            ),
            'time': data_time,
            'fields': dict(
                fields,
                channel=device['Channel'],
            ),
        })

# Internet connection
data_time = datetime.now(pytz.utc)
dsl_lines = sysbus.requete(
    'NeMo.Intf.data:getMIBs', {'traverse': 'all', 'mibs': 'dsl'})['status']
for dsl_interface, dsl_line in dsl_lines['dsl'].items():
    dsl_stats = sysbus.requete('NeMo.Intf.{interface}:getDSLStats'.format(
        interface=dsl_interface))['status']

    points.append({
        'measurement': 'livebox_dsl',
        'tags': {
            'status': dsl_line['LinkStatus'],
            'interface': dsl_interface,
            'type': dsl_line['ModulationType'],
        },
        'time': data_time,
        'fields': {
            'status': dsl_line['LinkStatus'],
            'uptime': dsl_line['LastChange'],
            'down_speed': dsl_line['DownstreamCurrRate'],
            'down_max_speed': dsl_line['DownstreamMaxRate'],
            'up_speed': dsl_line['UpstreamCurrRate'],
            'up_max_speed': dsl_line['UpstreamMaxRate'],
            'down_attenuation': dsl_line['DownstreamLineAttenuation'],
            'down_noise_margin': dsl_line['DownstreamNoiseMargin'],
            'up_attenuation': dsl_line['UpstreamLineAttenuation'],
            'up_noise_margin': dsl_line['UpstreamNoiseMargin'],
            'atuc_crc_errors': dsl_stats['ATUCCRCErrors'],
            'atuc_fec_errors': dsl_stats['ATUCFECErrors'],
            'atuc_hec_errors': dsl_stats['ATUCHECErrors'],
            'crc_errors': dsl_stats['CRCErrors'],
            'fec_errors': dsl_stats['FECErrors'],
            'hec_errors': dsl_stats['HECErrors'],
            'receive_blocks': dsl_stats['ReceiveBlocks'],
            'transmit_blocks': dsl_stats['TransmitBlocks'],
        },
    })

influxdb_client.write_points(points)
